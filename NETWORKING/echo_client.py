# -*- coding: utf-8 -*-
"""
Created on Sat Nov 28 10:33:09 2020

echo Client with sockets

Harry_Lab_book_April2020

Run this in a terminal whilst echo_server.py is running in second terminal


@author: DaddyDog
"""

import socket

HOST = '127.0.0.1'  # The server's hostname or IP address
PORT = 65431        # The port used by the server
MESSAGE = b'Hello Mr. Server, please send this message back'

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((HOST, PORT))
    s.sendall(MESSAGE)
    peer = s.getsockname()
    data = s.recv(1024) 

print('Received {} from Sender {}'.format( repr(data), repr(peer)))