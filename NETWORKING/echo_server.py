# -*- coding: utf-8 -*-
"""
Created on Sat Nov 28 10:31:12 2020

echo Server with sockets
Run this in a terminal whilst echo_client.py is running in second terminal


Harry_Lab_book_April2020


@author: DaddyDog
"""

import socket

HOST = '127.0.0.1'  # Standard loopback interface address (localhost)
PORT = 65431        # Port to listen on (non-privileged ports are > 1023)

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((HOST, PORT))
    s.listen()
    print('The server is about to block, listening on port  ', PORT)
    conn, addr = s.accept()  # blocks, waiting for incoming connection
    print('The server has heard a Client request on = ', addr)
    print('The sever reponds to the Client with its address and NEW port \n', conn)
    with conn:       
        while True:
            data = conn.recv(1024)
            print("Server recv'd ", data)
            if not data:
                break  # break once new data stops
            conn.sendall(data)
            print("Here")