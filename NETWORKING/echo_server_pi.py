# -*- coding: utf-8 -*-
"""
Created on Sat Nov 28 12:33:27 2020

echo Server with sockets
Run this on laptop whilst echo_client.py is running on Pi 


Harry_Lab_book_April2020


@author: DaddyDog
"""

import socket

HOST = '192.168.1.230'  # Standard loopback interface address (localhost)
PORT = 12345        # Port to listen on (non-privileged ports are > 1023)

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((HOST, PORT))
    s.listen()
    print('The server is about to block, listening on port  ', PORT)
    conn, addr = s.accept()  # blocks, waiting for incoming connection
    print('The server has heard a Client request on = ', addr)
    print('The sever responds to the Client with its address and NEW port \n', conn)
    with conn:       
        while True:
            data = conn.recv(1024)
            print("Server recv'd ", data)
            if not data:
                break  # break once new data stops
            conn.sendall(data)
  