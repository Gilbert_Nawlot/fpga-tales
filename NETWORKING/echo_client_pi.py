# -*- coding: utf-8 -*-
"""
Created on Sat Nov 28 12:35:15 2020


echo Client with sockets

Harry_Lab_book_April2020

Run this on a Pi whilst echo_server.py is running in laptop.


@author: DaddyDog
"""

import socket

HOST = '192.168.1.230'   # The server's hostname or IP address
PORT = 12345        # The port used by the server
MESSAGE = b'Hello Mr. Laptop, please send this message back'

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((HOST, PORT))
    s.sendall(MESSAGE)
    peer = s.getsockname()
    data = s.recv(1024) 

print('Received {} from Sender {}'.format( repr(data), repr(peer)))