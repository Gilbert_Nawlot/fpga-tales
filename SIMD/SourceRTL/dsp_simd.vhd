library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity dsp_simd is
generic (
  W : integer := 12
  );
port (
   clk : in std_logic;
   a0 : in unsigned(W-1 downto 0);
   b0 : in unsigned(W-1 downto 0);
   a1 : in unsigned(W-1 downto 0);
   b1 : in unsigned(W-1 downto 0);
   a2 : in unsigned(W-1 downto 0);
   b2 : in unsigned(W-1 downto 0);
   a3 : in unsigned(W-1 downto 0);
   b3 : in unsigned(W-1 downto 0);
   out0 : out unsigned(W-1 downto 0);
   out1 : out unsigned(W-1 downto 0);
   out2 : out unsigned(W-1 downto 0);
   out3 : out unsigned(W-1 downto 0)       
   );
end dsp_simd;

architecture arch of dsp_simd is

--attribute use_dsp : string;
--attribute use_dsp of arch : architecture is "simd";

 signal a0_r : unsigned(W-1 downto 0);
 signal b0_r : unsigned(W-1 downto 0);
 signal a1_r : unsigned(W-1 downto 0);
 signal b1_r : unsigned(W-1 downto 0);
 signal a2_r : unsigned(W-1 downto 0);
 signal b2_r : unsigned(W-1 downto 0);
 signal a3_r : unsigned(W-1 downto 0);
 signal b3_r : unsigned(W-1 downto 0);


begin

  process(clk)
  begin
    if rising_edge(clk) then 
      a0_r <= a0;
      b0_r <= b0;
      a1_r <= a1;
      b1_r <= b1;
      a2_r <= a2;
      b2_r <= b2;
      a3_r <= a3;
      b3_r <= b3;
      out0 <= a0_r + b0_r; 
      out1 <= a1_r + b1_r; 
      out2 <= a2_r + b2_r;   
      out3 <= a3_r + b3_r; 
    end if;
  end process;
      
end arch;
