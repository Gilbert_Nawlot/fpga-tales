# -*- coding: utf-8 -*-
"""
Created on Sat Nov 14 16:52:08 2020

@author: DaddyDog
"""

from functools import wraps


def my_logger(orig_func, LOG_NAME = "log.txt"):
    import logging
   # logging.basicConfig(filename='{}.log'.format(orig_func.__name__), level=logging.INFO)
    logging.basicConfig(filename = LOG_NAME , level=logging.INFO, filemode='w')
    @wraps(orig_func)
    def wrapper(*args, **kwargs):
        logging.info(
            ' {}({},{})'.format(orig_func.__name__, args, kwargs))
        ret_val = orig_func(*args, **kwargs)
        if ret_val is not None:
            logging.info(" returning: {}".format(ret_val))
        logging.info("")
        logging.info("")
        return ret_val
    logging.shutdown()
    return wrapper
