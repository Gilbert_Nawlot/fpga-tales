# -*- coding: utf-8 -*-
"""
Created on Sat Nov 14 11:57:43 2020

https://www.youtube.com/watch?v=FsAPt_9Bf3U

@author: DaddyDog
"""

from functools import wraps

LOG_NAME = "log.txt"

def my_logger(orig_func):
    import logging
   # logging.basicConfig(filename='{}.log'.format(orig_func.__name__), level=logging.INFO)
    logging.basicConfig(filename = LOG_NAME , level=logging.INFO, filemode='w')
    @wraps(orig_func)
    def wrapper(*args, **kwargs):
        logging.info(
            ' {}({},{})'.format(orig_func.__name__, args, kwargs))
        ret_val = orig_func(*args, **kwargs)
        if ret_val is not None:
            logging.info(" returning: {}".format(ret_val))
        logging.info("")
        logging.info("")
    logging.shutdown()
    return wrapper



@my_logger
def mult(a, b):
    p = a*b
    return p
    

@my_logger
def sum(a,b):
    s = a + b
    return s   


mult(2, 2)
sum(1, 2)
print('Done')