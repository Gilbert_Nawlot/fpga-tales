# -*- coding: utf-8 -*-
"""
Created on Sat Nov 14 16:50:17 2020

GEMM.py

@author: DaddyDog
"""
#import declog
import numpy as np
from declog import my_logger
import sys


# Calculate N_superscript'i_subscript'mn =
# i'th bit position of element at row m and column k of matrix 

@my_logger
def mx_sup_i_sub_mn(M,i,m,k):
    # extract the array value and bit shift the relevant bit out  
    return (M[m][k]) >> i & 1


@my_logger
def algorithm1(L,l,R,r):
    M, K  = L.shape
    N, _K = R.shape
    if K != _K:
        sys.exit("Inner dimensions must match for matrix product")
    else:
        P = np.zeros(shape=(M,N), dtype=int)
        for i in range(0,l):
            print(" Bit number in {} in L". format(i))
            for j in range(0,r):
                print("    Bit number {} in R". format(j))
                sgnL = -1 if i == l-1 else 1
                sgnR = -1 if i == r-1 else 1
                weight = sgnL*sgnR*2**(i+j)
                print("       weight {}".format(weight))
            # Binary matrix multiplication between L_supscript'i and R_supscript'i
            # L_superscript'i_subscript'mn refers to i'th bit position of element
            # ar row m and column k of matrix L
                for m in range(0,M):
                    print("          m = {}".format(m))
                    for n in range(0,N):
                        print("            n = {} ".format(n))
                        for k in range(0,K):
                             print("               k = {} ".format(k))
                             L_i_mk = mx_sup_i_sub_mn(L,i,m,k)
                             print("               L_i_mk = {}".format(L_i_mk) )
                             R_j_kn = mx_sup_i_sub_mn(R,j,k,n)
                             print("               R_j_kn = {}".format(R_j_kn))
                             P[M-1][N-1] += weight * L_i_mk * R_j_kn   
    return P

if __name__ == "__main__":
    
    # L, LEFT MATRIX
    M = 2  # rows
    K = 2  # cols
    l = 2  # num bits
    
    # R, RIGHT MATRIX
    K = 2  # rows
    N = 2  # cols
    r = 2  # num bits
    
    
    L = np.random.randint(low=0,high=2**l, size=(M,K), dtype=np.uint8)
    R = np.random.randint(low=0,high=2**r, size=(K,N), dtype=np.uint8)
   # W = np.array(np.ones((2,2)) , dtype=np.uint8)
    print(np.vectorize(np.binary_repr)(L, width=l))
  
    print("L = {}".format(np.vectorize(np.binary_repr)(L, width=l)))
    print("R = {}".format(np.vectorize(np.binary_repr)(R, width=r)))
    
    P = algorithm1(L,l,R,r)
    print("L = {}".format(L))
    print("L = {}".format(np.vectorize(np.binary_repr)(L, width=l)))
    print("R = {}".format(R))
    print("R = {}".format(np.vectorize(np.binary_repr)(R, width=r)))
    print("P = {}".format(P))
    print("P = {}".format(np.vectorize(np.binary_repr)(P, width=2*l*r)))
    
    
    
    